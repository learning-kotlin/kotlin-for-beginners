package org.elu.kotlin.beginning.ch10.oop

class House(val height: Double, val color: String, val price: Int) {
    fun print() {
        println("House [height=$height, color=$color, price=$price]")
    }
}

class House2(val height: Double, val color: String, val price: Int = 50000) {
    fun print() {
        println("House [height=$height, color=$color, price=$price]")
    }
}

fun main(args: Array<String>) {
    val house = House(4.5, "Blue", 150000)
    house.print()
    val house2 = House(height = 5.0, color = "Green", price = 200000)
    house2.print()
    val redHouse = House(color = "Red", price = 250000, height = 5.5)
    redHouse.print()
    val yellowHouse = House2(color = "Yellow", height = 2.5)
    yellowHouse.print()
}
