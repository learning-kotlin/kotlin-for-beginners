package org.elu.kotlin.beginning.ch10.oop

class Person {
    var name: String = "Janna"
    var age: Int = 51

    fun speak() {
        println("Привет")
    }

    fun greet(name: String) {
        println("Moi, $name!")
    }

    fun getYearOfBirth() = 2017 - age
}

class Person2(val name: String, var age: Int) {
    init {
        println("Object was created")
    }
    fun speak() {
        println("Terve")
    }

    fun greet(name: String) {
        println("Moi, $name!")
    }

    fun getYearOfBirth() = 2017 - age
}

fun main(args: Array<String>) {
    val person = Person()
    println(person.name)
    println(person.age)

    person.speak()
    person.greet("Sakari")
    println(person.getYearOfBirth())

    val person2 = Person2("Arno", 27)
    println(person2.name)
    println(person2.age)

    person2.speak()
    person2.greet("Lauri")
    println(person2.getYearOfBirth())
}
