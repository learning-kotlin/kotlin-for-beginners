package org.elu.kotlin.beginning.ch10.oop

open class PersonOpen(open val name: String, open var age: Int) {
    init {
        println("Object was created")
    }

    fun speak() {
        println("Привет")
    }

    fun greet(name: String) {
        println("Moi, $name!")
    }

    fun getYearOfBirth() = 2017 - age
}

abstract class PersonAbstract(open val name: String, open var age: Int) {
    init {
        println("Object was created")
    }

    abstract fun speak()

    fun greet(name: String) {
        println("Moi, $name!")
    }

    fun getYearOfBirth() = 2017 - age
}

class Student(override val name: String, override var age: Int, val studentId: Long): PersonOpen(name, age) {
    fun isIntelligent() = true
}

class Employee(override val name: String, override var age: Int): PersonOpen(name, age) {
    fun receivePayment() {
        println("Received payments")
    }
}

class Student2(override val name: String, override var age: Int, val studentId: Long): PersonAbstract(name, age) {
    override fun speak() {
        println("Moi, olen opiskelija")
    }

    fun isIntelligent() = true
}

class Employee2(override val name: String, override var age: Int): PersonAbstract(name, age) {
    override fun speak() {
        println("Terve, olen työntekijä")
    }

    fun receivePayment() {
        println("Received payments too")
    }
}

fun main(args: Array<String>) {
    val person = PersonOpen("Janna", 50)
    println(person.name)
    println(person.age)

    person.speak()
    person.greet("Sakari")
    println(person.getYearOfBirth())

    val student = Student("Arno", 27, 23456789)
    student.speak()
    println(student.isIntelligent())
    println(student.getYearOfBirth())

    val employee = Employee("Lauri", 23)
    employee.speak()
    employee.receivePayment()
    println(employee.getYearOfBirth())

    val student2 = Student2("Alisa", 22, 123456789)
    student2.speak()
    println(student2.isIntelligent())
    println(student2.getYearOfBirth())

    val employee2 = Employee2("Karoliina", 17)
    employee2.speak()
    employee2.receivePayment()
    println(employee2.getYearOfBirth())
}
