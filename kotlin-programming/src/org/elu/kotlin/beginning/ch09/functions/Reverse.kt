package org.elu.kotlin.beginning.ch09.functions

fun reverse(list: List<Int>): List<Int> {
    val result = arrayListOf<Int>()
    for (i in 0 until list.size) {
        result.add(list[list.size - 1 - i])
    }
    return result
}

fun reverse2(list: List<Int>): List<Int> {
    val result = arrayListOf<Int>()
    for (i in list.size - 1 downTo 0) {
        result.add(list[i])
    }
    return result
}

fun main(args: Array<String>) {
    val numbers = listOf(1, 2, 3, 5, 6, 8, 9)
    println(reverse(numbers))
    println(reverse2(numbers))
}