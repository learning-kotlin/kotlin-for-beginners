package org.elu.kotlin.beginning.ch14.io

import java.io.File

fun main(args: Array<String>) {
    val ipMap = mutableMapOf<String, Int>()
    File("src/ips.txt").forEachLine {
        val value = ipMap[it] ?: 0
        // alternative version could be
        // val value = ipMap.getOrDefault(it, 0)
        ipMap[it] = value + 1
    }
    val sorted = ipMap.toList().sortedByDescending { (_, value) -> value }.toMap()
    println(sorted.entries.take(10))

    // to get max count with one liners
    val ipMap2 = mutableMapOf<String, Int>()
    File("src/ips.txt").forEachLine {
        ipMap2[it] = (ipMap2[it] ?: 0) + 1
    }
    val (maxIp, maxCount) = ipMap.entries.maxBy { it.value }!!
    println("Most frequent IP address $maxIp, which occurred $maxCount times.")
}
