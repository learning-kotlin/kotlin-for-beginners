package org.elu.kotlin.beginning.ch14.io

fun main(args: Array<String>) {
    val namesToAges = mapOf("Edu" to 49, "Arno" to 27)

    println(namesToAges.keys)
    println(namesToAges.values)
    println(namesToAges.entries)

    val countryToInhabitants = mutableMapOf(
            "Germany" to 80_000_000,
            "Finland" to 5_000_000
    )
    countryToInhabitants.put("Australia", 23_000_000)
    countryToInhabitants.putIfAbsent("Finland", 4_800_000)

    println(countryToInhabitants.entries)
    println(countryToInhabitants.contains("Australia"))
    println(countryToInhabitants.containsKey("France"))
    println(countryToInhabitants.containsValue(20_000_000))

    println(countryToInhabitants.get("Finland"))
    println(countryToInhabitants.getOrDefault("France", 0))

    namesToAges.entries.forEach {
        println("${it.key} is ${it.value} years old.")
    }
}
