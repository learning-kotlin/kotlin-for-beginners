package org.elu.kotlin.beginning.ch14.io

import java.util.*

fun main(args: Array<String>) {
    val numnber = Random().nextInt(100) + 1
    var input: String?
    var guess = -1

    while (guess != numnber) {
        print("Guess a number between 1 and 100: ")
        input = readLine()

        if (input != null) {
            // can throw NumberFormatException
            guess = input.toInt()
        }
        if (guess < numnber) {
            println("Too low")
        } else if (guess > numnber) {
            println("Too large")
        } else {
            println("You won!")
        }
    }
}

