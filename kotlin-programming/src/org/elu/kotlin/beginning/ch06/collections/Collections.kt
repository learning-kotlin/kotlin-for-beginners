package org.elu.kotlin.beginning.ch06.collections

fun main(args: Array<String>) {
    // Arrays
    val array = arrayOf("Texas", "Iowa", "California")
    val mixed = arrayOf("Kotlin", 5, 3.0, false)
    val numbers = intArrayOf(1, 2, 3, 4, 5)
    println(mixed[0])
    mixed[2] = 3.1415
    println(mixed[2])
    val str = "Safari"
    println(str[0])

    val states = arrayOf("Nevada", "Florida")
    val allStates = array + states
    println(allStates.size)
    println(numbers.isEmpty())

    if (states.contains("Kentucky")) {
        println("it contains Kentucky")
    } else {
        println("it does not")
    }

    // Lists
    val list = listOf("Orange", "Apple", "Bananas")
    println(list)

    val arrayList = arrayListOf("Крош", "Хрюша", "Лосяш")
    arrayList.add("Копатыч")
    println(arrayList[0])
    val arrayList2 = arrayListOf("Бараш")
    println(arrayList + arrayList2)
    println(arrayList.size)
    println(arrayList.isEmpty())
    println(arrayList.contains("Крош"))

    arrayList.add(1, "Бараш")
    println(arrayList)

    var removed = arrayList.remove("Крош")
    println(arrayList)
    println(removed)
    removed = arrayList.remove("Edu")
    println(removed)

    val subList = arrayList.subList(1, 3)
    println(arrayList)
    println(subList)
}