package org.elu.kotlin.beginning.ch13.oop3

class Animal {
    var age = 0
        set(value) {
            if (value >= 0) {
                field = value
            }
        }
}

open class Animal2 {
    private var age: Int = 0
    protected var name = "Merlin"
    internal val isDangerous = true

    // public is default modifier and doesn't need to be defined explicitly
    fun isOld(): Boolean {
        return age > 12
    }
}

class Vertebrate: Animal2() {
    fun introduceYourself() {
        println(this.name)
    }
}

fun main(args: Array<String>) {
    val animal = Animal()
    animal.age = 8
    println(animal.age)
    animal.age = -2
    println(animal.age)

    val v = Vertebrate()
    v.introduceYourself()
}
