package org.elu.kotlin.beginning.ch13.oop3

// DRY = don't repeat yourself
class Stack<T>(vararg  items: T) {
    private val elements = items.toMutableList()

    fun push(element: T) {
        elements.add(element)
    }

    fun pop(): T? {
        if (!isEmpty()) {
            return elements.removeAt(elements.size - 1)
        }
        return null
    }

    fun isEmpty(): Boolean {
        return elements.isEmpty()
    }
}

fun <T> stackOf(vararg elements: T): Stack<T> {
    return Stack(*elements)
}

fun main(args: Array<String>) {
    val stack = Stack(3, 5, 2, 8)
    println(stack.push(11))
    println(stack.pop())
    println(stack.pop())
    println(stack.pop())
    println(stack.pop())
    println(stack.pop())

    val stack2 = stackOf("Hi", "Hey", "Hello")
    for (i in 1..3) {
        println(stack2.pop())
    }
}
