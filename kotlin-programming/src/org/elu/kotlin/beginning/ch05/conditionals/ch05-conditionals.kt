package org.elu.kotlin.beginning.ch05.conditionals

fun main(args: Array<String>) {
    val age = 21
    if (age < 18) {
        println("You cannot register.")
    } else if (age < 21) {
        println("Well, maybe you can register.")
    } else {
        println("You're good to go!")
    }

    var mode = 5
    when (mode) {
        1 -> println("The mode is lazy.")
        2 -> {
            println("The mode is 2")
            println("So the mode is busy")
        }
        3 -> println("This mode is super productive")
        else -> println("I don't know this mode")
    }

    mode = 2
    val result = when (mode) {
        1 -> "The mode is lazy."
        2 -> {
            println("The mode is 2")
            "So the mode is busy"
        }
        3 -> "This mode is super productive"
        else -> "I don't know this mode"
    }
    println(result)

    val x = if (mode < 2) {
        println("Mode is less than 2")
        17
    } else {
        42
    }
    println(x)

    val y = 10
    when (y) {
        5 -> println("x is 5")
        3*12 -> println("x is 3*12")
        "Hey there".length -> println("x is the length of string 'Hey there'")
        in 1..10 -> println("x is between 1 and 10")
        in 11..20 -> println("11 to 20")
        in 21..30 -> println("21 to 30")
        !in 1..9 -> println("x is not between 1 to 9")
    }
}
