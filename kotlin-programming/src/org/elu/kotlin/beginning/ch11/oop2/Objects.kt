package org.elu.kotlin.beginning.ch11.oop2

object Cache {
    val name = "HyperCache"

    fun retrieveData(): Int {
        return 42
    }
}

fun main(args: Array<String>) {
    println(Cache.name)
    println(Cache.retrieveData())
}
