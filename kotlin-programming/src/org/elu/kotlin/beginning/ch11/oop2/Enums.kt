package org.elu.kotlin.beginning.ch11.oop2

enum class Color {
    RED, BLUE, GREEN
}

class Car(val color: Color) {
    val timeRequired = 120

    fun drive() {
        println("Driving car...")
    }
}

fun main(args: Array<String>) {
    val car = Car(Color.BLUE)
    println(car.color)
    car.drive()
}

