package org.elu.kotlin.beginning.ch11.oop2

abstract class Course(val topic: String, val price: Double) {
    open fun learn() {
        println("Learning a great $topic course.")
    }
}

interface Learnable {
    fun learn() {
        println("Learning...")
    }
}

open class KotlinCourse: Course("Kotlin", 99.99), Learnable {
    final override fun learn() {
        super<Course>.learn()
        super<Learnable>.learn()
        println("I'm one of the first person to learn Kotlin!")
    }
}

class SpecialKotlinCourse: KotlinCourse() {
}

fun main(args: Array<String>) {
    val kotlinCourse = KotlinCourse()
    kotlinCourse.learn()
    val specialKotlinCourse = SpecialKotlinCourse()
    specialKotlinCourse.learn()
}
