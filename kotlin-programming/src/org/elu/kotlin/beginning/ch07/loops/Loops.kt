package org.elu.kotlin.beginning.ch07.loops

fun main(args: Array<String>) {
    var sum = 0
    for (i in 1..100) {
        sum += i
    }
    println(sum)

    val list = listOf("Java", "Kotlin", "Groovy")
    for (element in list) {
        println(element)
    }

    for ((index, value) in list.withIndex()) {
        println("Element at $index is $value")
    }

    var x = 9
    while (x >= 0) {
        println(x)
        x--
    }

    var y = 1
    while (y <= 10) {
        println(y)
        y++
    }

    for (i in 1..10) {
        for (j in 1..10) {
            if (i - j == 5) {
                break
            }
            println("$i - $j")
        }
    }
    println("===================")

    outer@ for (i in 1..10) {
        for (j in 1..10) {
            if (i - j == 5) {
                break@outer
            }
            println("$i - $j")
        }
    }
}